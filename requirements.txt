django==3.*
#mysqlclient==1.4.6
pymysql==1.0.2
dj-database-url
python-decouple
requests

git+https://github.com/karimbahgat/django-wkb
shapefile==2.2.*
shapely==1.*
dateparser==1.1.*
django-jsonform
